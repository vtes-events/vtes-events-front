export const environment = {
  domain: 'localhost',
  apiDomain: 'localhost:8000',
  production: false,
  api: {
    baseUrl: 'http://localhost:8000/api',
  },
  recaptcha: {
    siteKey: '6LfJv20lAAAAADlLV3ZKWn8KKld41mxQsELMx591',
  },
  googleAnalytics: {
    trackingId: 'G-W2QXB3J46Z',
  },
};
