export const environment = {
  domain: 'vtes.events',
  apiDomain: 'api.vtes.events',
  production: true,
  api: {
    baseUrl: 'https://api.vtes.events/api',
  },
  recaptcha: {
    siteKey: '6LfJv20lAAAAADlLV3ZKWn8KKld41mxQsELMx591',
  },
  googleAnalytics: {
    trackingId: 'G-W2QXB3J46Z',
  },
};
