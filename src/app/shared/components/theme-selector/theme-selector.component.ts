import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ColorSchemeService, MediaService } from '../../services';

@Component({
  selector: 'app-theme-selector',
  templateUrl: './theme-selector.component.html',
  styleUrls: ['./theme-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeSelectorComponent implements OnInit {
  isMobile$!: Observable<boolean>;
  icon!: string;

  constructor(
    private colorSchemeService: ColorSchemeService,
    private mediaService: MediaService
  ) {}

  ngOnInit() {
    this.isMobile$ = this.mediaService.observeMobile();
    this.icon =
      this.colorSchemeService.currentActive() === 'dark'
        ? 'light_mode'
        : 'dark_mode';
  }

  toggleTheme(): void {
    if (this.colorSchemeService.currentActive() === 'dark') {
      this.colorSchemeService.update('light');
      this.icon = 'dark_mode';
    } else {
      this.colorSchemeService.update('dark');
      this.icon = 'light_mode';
    }
  }
}
