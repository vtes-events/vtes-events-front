import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { MediaService } from '../../services/media.service';
interface Language {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguageSelectorComponent implements OnInit {
  languages: Language[] = [
    { value: 'en', viewValue: 'English' },
    { value: 'es', viewValue: 'Español' },
  ];
  selectedLanguages = this.languages[0].value;

  isMobile$!: Observable<boolean>;

  constructor(
    private translocoService: TranslocoService,
    private mediaService: MediaService
  ) {}

  ngOnInit() {
    this.selectedLanguages = this.translocoService.getActiveLang();
    this.isMobile$ = this.mediaService.observeMobile();
  }

  changeLanguage(languageCode: string): void {
    this.selectedLanguages = languageCode;
    this.translocoService.setActiveLang(languageCode);
  }
}
