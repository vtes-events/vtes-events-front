import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { UserService } from '../../state';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidenavComponent implements OnInit {
  @Output() closeSidenav: EventEmitter<boolean> = new EventEmitter();

  constructor(private userService: UserService) {}

  ngOnInit() {}

  signOut(): void {
    this.userService.signOut();
  }

  close(): void {
    this.closeSidenav.emit();
  }
}
