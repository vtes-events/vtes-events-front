import { Observable } from 'rxjs';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { UserService } from '../../state';
import { MediaService } from '../../services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  @Output() toggleSidenav: EventEmitter<boolean> = new EventEmitter();

  isMobile$!: Observable<boolean>;

  constructor(
    private mediaService: MediaService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.isMobile$ = this.mediaService.observeMobile();
  }

  signOut(): void {
    this.userService.signOut();
  }
}
