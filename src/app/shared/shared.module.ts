import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TranslocoModule } from '@ngneat/transloco';
import { LanguageSelectorComponent } from './components/language-selector/language-selector.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { IsLoggedDirective } from './directives';
import { ThemeSelectorComponent } from './components/theme-selector/theme-selector.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NewVersionDialogComponent } from './components/new-version-dialog/new-version-dialog.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MapErrorPipe } from './pipes/transloco-error/map-error.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslocoModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
  ],
  declarations: [
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    LanguageSelectorComponent,
    ThemeSelectorComponent,
    IsLoggedDirective,
    NewVersionDialogComponent,
    PageNotFoundComponent,
    MapErrorPipe,
  ],
  exports: [
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    IsLoggedDirective,
    NewVersionDialogComponent,
    PageNotFoundComponent,
    MapErrorPipe,
  ],
})
export class SharedModule {}
