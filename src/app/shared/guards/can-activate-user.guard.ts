import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  catchError,
  map,
  Observable,
  of,
  switchMap,
  tap,
  throwError,
} from 'rxjs';
import { UserQuery, UserService } from '../state';

@Injectable({
  providedIn: 'root',
})
export class CanActivateUser {
  constructor(
    private userQuery: UserQuery,
    private userService: UserService,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> {
    return this.userService.checkAccessToken().pipe(
      switchMap(() => this.userQuery.selectAuthenticated()),
      catchError((error) => {
        this.router.navigate(['/']);
        return throwError(() => error);
      })
    );
  }
}
