import { UserQuery } from './../state/user/user.query';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import {
  ChangeDetectorRef,
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { tap } from 'rxjs';

@UntilDestroy()
@Directive({
  selector: '[isLogged]',
})
export class IsLoggedDirective {
  private hasView = false;

  constructor(
    private userQuery: UserQuery,
    private templateRef: TemplateRef<unknown>,
    private viewContainer: ViewContainerRef,
    private changes: ChangeDetectorRef
  ) {}

  @Input() set isLogged(logged: boolean) {
    this.userQuery
      .selectAuthenticated()
      .pipe(
        untilDestroyed(this),
        tap((authenticated) => {
          const condition = logged ? authenticated : !authenticated;
          if (condition && !this.hasView) {
            this.viewContainer.createEmbeddedView(this.templateRef);
            this.hasView = true;
            this.changes.markForCheck();
          } else if (!condition && this.hasView) {
            this.viewContainer.clear();
            this.hasView = false;
            this.changes.markForCheck();
          }
        })
      )
      .subscribe();
  }
}
