export interface ApiUser {
  username: string;
  email: string;
  veknId: string;
  firstName: string;
  lastName: string;
}
