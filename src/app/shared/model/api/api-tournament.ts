import { ApiShowTypePolicy } from './api-show-deck-policy.enum';

export interface ApiTournament {
  id: number;
  playerCount: number;
  eventId: string;
  organizerVeknId: string;
  name: string;
  email?: string;
  description?: string;
  date: string;
  time: string;
  venueName?: string;
  address: string;
  city: string;
  country: string;
  registrationFee: string;
  proxiesAllowed: boolean;
  rounds: string;
  phoneNumber?: string;
  website?: string;
  eventType: string;
  registrationOpen?: boolean;
  showDeckPolicy: ApiShowTypePolicy;
  isRegistered?: boolean;
}
