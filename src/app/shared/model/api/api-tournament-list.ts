import { ApiTournament } from './api-tournament';

export interface ApiTournamentList {
  count: number;
  next: string;
  previous: string;
  results: ApiTournament[];
}
