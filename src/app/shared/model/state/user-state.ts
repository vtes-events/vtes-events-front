export interface UserState {
  username?: string;
  refresh?: string;
  access?: string;
  email?: string;
  veknId?: string;
  firstName?: string;
  lastName?: string;
}
