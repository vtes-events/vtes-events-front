import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ApiLogin, ApiTournament, ApiTournamentList, ApiUser } from '../model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class ApiDataService {
  private readonly loginPath = '/auth/login';
  private readonly tokenRefreshPath = '/auth/login/refresh';
  private readonly changePasswordPath = '/auth/password/change';
  private readonly usersPath = '/users';
  private readonly usersSelfPath = '/users/self';
  private readonly tournamentsPath = '/tournaments';
  private readonly tournamentsDetailPath = '/tournaments/';
  private readonly contactPath = '/contact';

  constructor(private httpClient: HttpClient) {}

  login(
    username: string,
    password: string,
    captchaToken: string
  ): Observable<ApiLogin> {
    return this.httpClient.post<ApiLogin>(
      `${environment.api.baseUrl}${this.loginPath}`,
      {
        username,
        password,
        captchaToken,
      }
    );
  }

  tokenRefresh(refresh: string): Observable<ApiLogin> {
    return this.httpClient.post<ApiLogin>(
      `${environment.api.baseUrl}${this.tokenRefreshPath}`,
      {
        refresh,
      }
    );
  }

  createUser(
    username: string,
    email: string,
    veknId: string,
    password: string,
    captchaToken: string
  ): Observable<ApiLogin> {
    return this.httpClient.post<ApiLogin>(
      `${environment.api.baseUrl}${this.usersPath}`,
      {
        username,
        email,
        veknId,
        password,
        captchaToken,
      }
    );
  }

  changePassword(oldPassword: string, newPassword: string): Observable<void> {
    return this.httpClient.post<void>(
      `${environment.api.baseUrl}${this.changePasswordPath}`,
      {
        oldPassword,
        newPassword,
      }
    );
  }

  getUser(): Observable<ApiUser> {
    return this.httpClient.get<ApiUser>(
      `${environment.api.baseUrl}${this.usersSelfPath}`
    );
  }

  contact(
    name: string,
    email: string,
    message: string,
    captchaToken: string
  ): Observable<boolean> {
    return this.httpClient.post<boolean>(
      `${environment.api.baseUrl}${this.contactPath}`,
      {
        name,
        email,
        message,
        captchaToken,
      }
    );
  }

  getTournaments(
    from: Date,
    to: Date,
    offset: number,
    limit: number
  ): Observable<ApiTournamentList> {
    let params = new HttpParams();
    params = params.append('from', moment(from).format('YYYY-MM-DD'));
    params = params.append('to', moment(to).format('YYYY-MM-DD'));
    params = params.append('offset', offset);
    params = params.append('limit', limit);
    return this.httpClient.get<ApiTournamentList>(
      `${environment.api.baseUrl}${this.tournamentsPath}`,
      {
        params: params,
      }
    );
  }

  getTournament(id: number): Observable<ApiTournament> {
    return this.httpClient.get<ApiTournament>(
      `${environment.api.baseUrl}${this.tournamentsDetailPath}${id}`
    );
  }

  getVEKNTournament(id: number): Observable<ApiTournament> {
    return this.httpClient.get<ApiTournament>(
      `${environment.api.baseUrl}${this.tournamentsDetailPath}vekn/${id}`
    );
  }

  createTournament(tournament: ApiTournament): Observable<ApiTournament> {
    return this.httpClient.post<ApiTournament>(
      `${environment.api.baseUrl}${this.tournamentsPath}`,
      tournament
    );
  }

  registerTournament(id: number, decklist: string): Observable<any> {
    return this.httpClient.post<ApiTournament>(
      `${environment.api.baseUrl}${this.tournamentsDetailPath}${id}/registration`,
      {
        decklist,
      }
    );
  }

  deregisterTournament(id: number): Observable<any> {
    return this.httpClient.delete<ApiTournament>(
      `${environment.api.baseUrl}${this.tournamentsDetailPath}${id}/registration`,
      {}
    );
  }

  userVerify(id: number, token: string): Observable<any> {
    return this.httpClient.post<ApiTournament>(
      `${environment.api.baseUrl}${this.usersPath}/${id}/verify`,
      {
        token,
      }
    );
  }
}
