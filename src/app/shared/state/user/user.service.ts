import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import { UserStore } from './user.store';
import { ApiLogin, ApiUser } from '../../model';
import { ApiDataService } from '../../services';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private userStore: UserStore,
    private jwtHelper: JwtHelperService,
    private apiDataService: ApiDataService
  ) {}

  signIn(
    username: string,
    password: string,
    captchaToken: string,
    remember: boolean
  ): Observable<ApiLogin> {
    this.userStore.setLoading(true);
    return this.apiDataService.login(username, password, captchaToken).pipe(
      tap((response: ApiLogin) => {
        this.userStore.updateToken(response, remember);
        this.userStore.setLoading();
      }),
      catchError((error) => {
        this.userStore.setLoading();
        return throwError(() => error);
      })
    );
  }

  signOut(): void {
    this.userStore.updateToken({} as ApiLogin, false);
  }

  checkAccessToken(): Observable<ApiLogin> {
    if (this.jwtHelper.isTokenExpired()) {
      const refreshToken = this.userStore.getValue().refresh;
      if (refreshToken && !this.jwtHelper.isTokenExpired(refreshToken)) {
        return this.apiDataService.tokenRefresh(refreshToken).pipe(
          tap((response: ApiLogin) => {
            this.userStore.updateToken(response);
          }),
          catchError((error) => {
            this.signOut();
            return throwError(() => error);
          })
        );
      }
      this.signOut();
    }
    return of({});
  }

  getUserInfo(): Observable<ApiUser> {
    return this.apiDataService.getUser().pipe(
      tap((response: ApiUser) => {
        this.userStore.updateUser(response);
      })
    );
  }
}
