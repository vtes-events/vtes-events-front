import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { ApiLogin, ApiUser, UserState } from '../../model';
import { LocalStorageService, SessionStorageService } from '../../services';

const initialState: UserState = {};

@Injectable({
  providedIn: 'root',
})
@StoreConfig({ name: UserStore.storeName })
export class UserStore extends Store<UserState> {
  static readonly storeName = 'user';

  constructor(
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService
  ) {
    super(initialState);
    const previousState = this.sessionStorage.getValue<UserState>(
      UserStore.storeName
    );
    if (previousState) {
      this.update(previousState);
    }
    const previousLocalState = this.localStorage.getValue<UserState>(
      UserStore.storeName
    );
    if (previousLocalState) {
      this.update(previousLocalState);
      this.sessionStorage.setValue(UserStore.storeName, previousLocalState);
    }
    this.setLoading();
  }

  updateToken(response: ApiLogin, remember?: boolean): void {
    const { refresh, access } = response;
    this.update((state) => ({ ...state, refresh, access }));
    if (Boolean(remember) || this.localStorage.getValue(UserStore.storeName)) {
      this.localStorage.setValue(UserStore.storeName, this.getValue());
    } else {
      this.localStorage.clearValue(UserStore.storeName);
    }
    this.sessionStorage.setValue(UserStore.storeName, this.getValue());
  }

  updateUser(response: ApiUser): void {
    const { username, email, veknId, firstName, lastName } = response;
    this.update((state) => ({
      ...state,
      username,
      email,
      veknId,
      firstName,
      lastName,
    }));
    if (this.localStorage.getValue(UserStore.storeName)) {
      this.localStorage.setValue(UserStore.storeName, this.getValue());
    }
    this.sessionStorage.setValue(UserStore.storeName, this.getValue());
  }
}
