import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { UserStore } from './user.store';
import { UserState } from '../../model';

@Injectable({
  providedIn: 'root',
})
export class UserQuery extends Query<UserState> {
  constructor(protected override store: UserStore) {
    super(store);
  }

  selectAuthenticated(): Observable<boolean> {
    return this.select((user: UserState) => !!user.access || !!user.refresh);
  }

  selectAccessToken(): Observable<string | undefined> {
    return this.select((user: UserState) => user.access);
  }

  selectRefreshToken(): Observable<string | undefined> {
    return this.select((user: UserState) => user.refresh);
  }

  selectUsername(): Observable<string> {
    return this.select((user: UserState) => user.username ?? '');
  }

  selectFirstName(): Observable<string> {
    return this.select((user: UserState) => user.firstName ?? '');
  }

  selectLastName(): Observable<string> {
    return this.select((user: UserState) => user.lastName ?? '');
  }

  selectEmail(): Observable<string> {
    return this.select((user: UserState) => user.email ?? '');
  }

  selectVeknId(): Observable<string> {
    return this.select((user: UserState) => user.veknId ?? '');
  }

  isAuthenticated(): boolean {
    return !!this.getValue().access || !!this.getValue().refresh;
  }

  getAccessToken(): string | undefined {
    return this.getValue().access;
  }

  getRefreshToken(): string | undefined {
    return this.getValue().refresh;
  }
}
