import { TranslocoService } from '@ngneat/transloco';
import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of, switchMap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiError } from '../../model';

@Pipe({
  name: 'mapError',
})
export class MapErrorPipe implements PipeTransform {
  constructor(private transloco: TranslocoService) {}

  transform(value: HttpErrorResponse): Observable<string> {
    const apiError = value?.error as ApiError;
    console.warn(`${apiError?.error}: ${apiError?.message}`);
    return this.transloco
      .selectTranslate(apiError?.error, undefined, 'error')
      .pipe(
        switchMap((result) =>
          result === `error.${apiError?.error}`
            ? this.transloco.selectTranslate(
                'unexpected_error',
                undefined,
                'error'
              )
            : of(result)
        )
      );
  }
}
