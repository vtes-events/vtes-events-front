import { TranslocoService } from '@ngneat/transloco';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { Component, OnDestroy, OnInit, isDevMode } from '@angular/core';
import { UserService } from './shared/state/user/user.service';
import { ColorSchemeService } from './shared/services/color-scheme.service';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { Subscription, distinct, filter, tap, zip } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { NewVersionDialogComponent } from './shared/components/new-version-dialog/new-version-dialog.component';
import { NgcCookieConsentService } from 'ngx-cookieconsent';

@UntilDestroy()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'VTES Events';

  private popupOpenSubscription!: Subscription;
  private popupCloseSubscription!: Subscription;
  private initializingSubscription!: Subscription;
  private initializedSubscription!: Subscription;
  private initializationErrorSubscription!: Subscription;
  private statusChangeSubscription!: Subscription;
  private revokeChoiceSubscription!: Subscription;
  private noCookieLawSubscription!: Subscription;

  constructor(
    private userService: UserService,
    private colorSchemeService: ColorSchemeService,
    private swUpdate: SwUpdate,
    private dialog: MatDialog,
    private ccService: NgcCookieConsentService,
    private translocoService: TranslocoService
  ) {
    this.colorSchemeService.load();
  }

  ngOnInit() {
    //JWT token renew
    this.userService.checkAccessToken().pipe(untilDestroyed(this)).subscribe();
    //PWA update
    if (this.swUpdate.isEnabled) {
      this.swUpdate.versionUpdates
        .pipe(
          untilDestroyed(this),
          distinct(),
          tap((evt) => console.log(evt))
        )
        .subscribe();
      this.swUpdate.checkForUpdate();
    }
    //Cookie consent
    zip(
      this.translocoService.selectTranslate('cookie_consent.message'),
      this.translocoService.selectTranslate('cookie_consent.dismiss'),
      this.translocoService.selectTranslate('cookie_consent.link')
    ).subscribe((content) => {
      this.ccService.getConfig().content = {};
      this.ccService.getConfig().content!.message = content[0];
      this.ccService.getConfig().content!.dismiss = content[1];
      this.ccService.getConfig().content!.link = content[2];
      this.ccService.destroy();
      this.ccService.init(this.ccService.getConfig());
    });
    this.popupOpenSubscription = this.ccService.popupOpen$.subscribe();
    this.popupCloseSubscription = this.ccService.popupClose$.subscribe();
    this.initializingSubscription = this.ccService.initializing$.subscribe();
    this.initializedSubscription = this.ccService.initialized$.subscribe();
    this.initializationErrorSubscription =
      this.ccService.initializationError$.subscribe();
    this.statusChangeSubscription = this.ccService.statusChange$.subscribe();
    this.revokeChoiceSubscription = this.ccService.revokeChoice$.subscribe();
    this.noCookieLawSubscription = this.ccService.noCookieLaw$.subscribe();
    if (!isDevMode()) {
      let node = document.createElement('script');
      node.src = 'assets/js/new_relic.js';
      node.type = 'text/javascript';
      node.async = true;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  ngOnDestroy() {
    this.popupOpenSubscription.unsubscribe();
    this.popupCloseSubscription.unsubscribe();
    this.initializingSubscription.unsubscribe();
    this.initializedSubscription.unsubscribe();
    this.initializationErrorSubscription.unsubscribe();
    this.statusChangeSubscription.unsubscribe();
    this.revokeChoiceSubscription.unsubscribe();
    this.noCookieLawSubscription.unsubscribe();
  }
}
