import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiDataService, ApiTournament } from '../shared';
import { Observable, tap } from 'rxjs';
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeUrl,
} from '@angular/platform-browser';
import { UserQuery } from '../shared/state';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventComponent implements OnInit {
  event$!: Observable<ApiTournament>;
  mapUrl!: SafeResourceUrl;
  userVeknId$ = this.userQuery.selectVeknId();

  constructor(
    private route: ActivatedRoute,
    private apiDataService: ApiDataService,
    private sanitizer: DomSanitizer,
    private userQuery: UserQuery
  ) {}

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id')!);
    this.event$ = this.apiDataService
      .getTournament(id)
      .pipe(
        tap(
          (event) =>
            (this.mapUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
              `https://maps.google.com/maps?width=100%&height=600&hl=en&q=${
                event.address
              }+(${event.venueName!})&ie=UTF8&t=&z=14&iwloc=B&output=embed`
            ))
        )
      );
  }
}
