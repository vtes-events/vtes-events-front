import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiDataService, ApiTournament } from '../../shared';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HttpErrorResponse } from '@angular/common/http';

@UntilDestroy()
@Component({
  selector: 'app-event-registration',
  templateUrl: './event-registration.component.html',
  styleUrls: ['./event-registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventRegistrationComponent implements OnInit {
  id!: number;
  event$!: Observable<ApiTournament>;
  form!: FormGroup;
  error?: HttpErrorResponse;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiDataService: ApiDataService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id')!);
    this.event$ = this.apiDataService
      .getTournament(this.id)
      .pipe(untilDestroyed(this));
    this.form = new FormGroup({
      deck: new FormControl(null, Validators.required),
    });
  }

  get deck() {
    return this.form.get('deck');
  }

  register(): void {
    this.apiDataService
      .registerTournament(this.id, this.deck!.value)
      .pipe(untilDestroyed(this))
      .subscribe({
        complete: () => {
          this.redirectToEventPage();
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
        },
      })
      .add(() => {
        this.changeDetectorRef.detectChanges();
      });
  }

  deregister(): void {
    this.apiDataService
      .deregisterTournament(this.id)
      .pipe(untilDestroyed(this))
      .subscribe({
        complete: () => {
          this.redirectToEventPage();
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
        },
      })
      .add(() => {
        this.changeDetectorRef.detectChanges();
      });
  }

  private redirectToEventPage(): void {
    this.router.navigate([`/events/${this.id}`]);
  }
}
