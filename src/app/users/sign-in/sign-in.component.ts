import { Observable } from 'rxjs';
import { UserQuery, UserService } from './../../shared/state';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { RecaptchaComponent } from 'ng-recaptcha';

@UntilDestroy(this)
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInComponent implements OnInit {
  @ViewChild('captchaRef') captchaRef!: RecaptchaComponent;

  hidePassword = true;

  verify = false;

  userLoading$!: Observable<boolean>;

  form!: FormGroup;

  error?: HttpErrorResponse;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userQuery: UserQuery,
    private userService: UserService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required),
      remember: new FormControl(false),
    });
    this.userLoading$ = this.userQuery.selectLoading();
    this.activatedRoute.queryParams
      .pipe(untilDestroyed(this))
      .subscribe((params) => {
        this.verify = Boolean(params['verify']);
        this.changeDetectorRef.detectChanges();
      });
  }

  get username() {
    return this.form.get('username');
  }

  get password() {
    return this.form.get('password');
  }

  get remember() {
    return this.form.get('remember');
  }

  togglePassword(): void {
    this.hidePassword = !this.hidePassword;
  }

  signIn(captchaToken: string): void {
    if (!captchaToken) {
      return;
    }
    const { username, password, remember } = this.form.value;
    this.userService
      .signIn(username, password, captchaToken, remember)
      .subscribe({
        next: () => {
          this.router.navigate(['/']);
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
          this.verify = false;
          this.captchaRef.reset();
          this.changeDetectorRef.detectChanges();
        },
      });
  }
}
