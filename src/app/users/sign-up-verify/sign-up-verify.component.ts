import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiDataService } from '../../shared';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, map, of, switchMap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@UntilDestroy(this)
@Component({
  selector: 'app-sign-up-verify',
  templateUrl: './sign-up-verify.component.html',
  styleUrls: ['./sign-up-verify.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpVerifyComponent implements OnInit {
  error?: HttpErrorResponse;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiDataService: ApiDataService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams
      .pipe(
        untilDestroyed(this),
        filter((params) => params['token'] && params['user']),
        switchMap((params) =>
          this.apiDataService.userVerify(params['user'], params['token'])
        )
      )
      .subscribe({
        next: () => {
          this.router.navigate(['/users/sign_in'], {
            queryParams: {
              verify: true,
            },
          });
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
          this.changeDetectorRef.detectChanges();
        },
      });
  }
}
