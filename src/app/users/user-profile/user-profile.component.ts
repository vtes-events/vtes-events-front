import { UserService } from '../../shared/state';
import { UserQuery } from './../../shared/state/user/user.query';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { Observable, finalize } from 'rxjs';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiDataService } from '../../shared';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileComponent implements OnInit {
  username$!: Observable<string>;
  firstName$!: Observable<string>;
  lastName$!: Observable<string>;
  email$!: Observable<string>;
  veknId$!: Observable<string>;
  success = false;
  hideOldPassword = true;
  hideNewPassword = true;
  hideConfirmNewPassword = true;
  loading = false;
  error?: HttpErrorResponse;

  form!: FormGroup;

  constructor(
    private userQuery: UserQuery,
    private userService: UserService,
    private apiDataService: ApiDataService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.username$ = this.userQuery.selectUsername();
    this.firstName$ = this.userQuery.selectFirstName();
    this.lastName$ = this.userQuery.selectLastName();
    this.email$ = this.userQuery.selectEmail();
    this.veknId$ = this.userQuery.selectVeknId();
    this.userService.getUserInfo().subscribe();

    this.form = new FormGroup(
      {
        oldPassword: new FormControl(null, Validators.required),
        newPassword: new FormControl(null, [
          Validators.required,
          Validators.minLength(8),
          this.patternValidator(/\d/, { hasNumber: true }),
        ]),
        confirmNewPassword: new FormControl(null, [
          Validators.required,
          Validators.minLength(8),
          this.patternValidator(/\d/, { hasNumber: true }),
        ]),
      },
      { validators: this.passwordMatchValidator }
    );
  }

  toggleOldPassword(): void {
    this.hideOldPassword = !this.hideOldPassword;
  }

  toggleNewPassword(): void {
    this.hideNewPassword = !this.hideNewPassword;
  }

  toggleConfirmNewPassword(): void {
    this.hideConfirmNewPassword = !this.hideConfirmNewPassword;
  }

  get oldPassword() {
    return this.form.get('oldPassword');
  }

  get newPassword() {
    return this.form.get('newPassword');
  }

  get confirmNewPassword() {
    return this.form.get('confirmNewPassword');
  }

  private patternValidator(
    regex: RegExp,
    error: ValidationErrors
  ): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return {};
      }
      const valid = regex.test(control.value);
      return valid ? {} : error;
    };
  }

  private passwordMatchValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const password = control!.get('newPassword')?.value;
    const confirmPassword = control!.get('confirmNewPassword')?.value;
    if (password !== confirmPassword) {
      control!.get('confirmNewPassword')?.setErrors({ noPasswordMatch: true });
    }
    return null;
  }

  changePassword(): void {
    const { oldPassword, newPassword } = this.form.value;
    this.loading = true;
    this.apiDataService
      .changePassword(oldPassword, newPassword)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe({
        next: () => {
          this.form.reset();
          this.form.controls['oldPassword'].setErrors(null);
          this.form.controls['newPassword'].setErrors(null);
          this.form.controls['confirmNewPassword'].setErrors(null);
          this.success = true;
          this.error = undefined;
          this.changeDetectorRef.detectChanges();
          this.router.navigate([], {
            queryParams: {
              success: true,
            },
          });
        },
        error: (error: HttpErrorResponse) => {
          this.success = false;
          this.error = error;
          this.changeDetectorRef.detectChanges();
        },
      });
  }
}
