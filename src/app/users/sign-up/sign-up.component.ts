import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SignUpVeknIdDialogComponent } from '../sign-up-vekn-id-dialog/sign-up-vekn-id-dialog.component';
import { ApiDataService } from '../../shared';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpComponent implements OnInit {
  @ViewChild('captchaRef') captchaRef!: RecaptchaComponent;
  hidePassword = true;
  hideConfirmPassword = true;
  loading = false;
  error?: HttpErrorResponse;
  form!: FormGroup;

  constructor(
    private apiDataService: ApiDataService,
    private router: Router,
    private dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.form = new FormGroup(
      {
        username: new FormControl(null, [Validators.required]),
        email: new FormControl(null, [Validators.required, Validators.email]),
        veknId: new FormControl(null, [
          Validators.required,
          Validators.maxLength(7),
          Validators.pattern(/\d{7}/),
        ]),
        password: new FormControl(null, [
          Validators.required,
          Validators.minLength(8),
          this.patternValidator(/\d/, { hasNumber: true }),
        ]),
        confirmPassword: new FormControl(null, [
          Validators.required,
          Validators.minLength(8),
          this.patternValidator(/\d/, { hasNumber: true }),
        ]),
        terms: new FormControl(false, Validators.requiredTrue),
      },
      {
        validators: this.passwordMatchValidator,
      }
    );
  }

  get username() {
    return this.form.get('username');
  }

  get email() {
    return this.form.get('email');
  }

  get veknId() {
    return this.form.get('veknId');
  }

  get password() {
    return this.form.get('password');
  }
  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  get terms() {
    return this.form.get('terms');
  }

  togglePassword(): void {
    this.hidePassword = !this.hidePassword;
  }

  toggleConfirmPassword(): void {
    this.hideConfirmPassword = !this.hideConfirmPassword;
  }

  showVeknIdHelp(): void {
    this.dialog.open(SignUpVeknIdDialogComponent);
  }

  signUp(captchaToken: string): void {
    if (!captchaToken) {
      return;
    }
    const { username, email, veknId, password } = this.form.value;
    this.loading = true;
    this.apiDataService
      .createUser(username, email, veknId, password, captchaToken)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe({
        next: () => {
          this.router.navigate(['/users/sign_up/thank-you']);
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
          this.captchaRef.reset();
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  private patternValidator(
    regex: RegExp,
    error: ValidationErrors
  ): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return {};
      }
      const valid = regex.test(control.value);
      return valid ? {} : error;
    };
  }

  private passwordMatchValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const password = control!.get('password')?.value;
    const confirmPassword = control!.get('confirmPassword')?.value;
    if (password !== confirmPassword) {
      control!.get('confirmPassword')?.setErrors({ noPasswordMatch: true });
    }
    return null;
  }
}
