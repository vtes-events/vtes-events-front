import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-sign-up-thank-you',
  templateUrl: './sign-up-thank-you.component.html',
  styleUrls: ['./sign-up-thank-you.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpThankYouComponent {}
