import { NgModule, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { RouterModule, Routes } from '@angular/router';
import { TRANSLOCO_SCOPE, TranslocoModule } from '@ngneat/transloco';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { SignUpVeknIdDialogComponent } from './sign-up-vekn-id-dialog/sign-up-vekn-id-dialog.component';
import {
  RECAPTCHA_SETTINGS,
  RecaptchaModule,
  RecaptchaSettings,
} from 'ng-recaptcha';
import { environment } from '../../environments/environment';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CanActivateUser } from '../shared/guards/can-activate-user.guard';
import { SharedModule } from '../shared/shared.module';
import { SignUpThankYouComponent } from './sign-up-thank-you/sign-up-thank-you.component';
import { SignUpVerifyComponent } from './sign-up-verify/sign-up-verify.component';

const routes: Routes = [
  {
    path: 'sign_in',
    component: SignInComponent,
    title: 'Sign in · VTES Events',
  },
  {
    path: 'sign_up',
    children: [
      {
        path: '',
        component: SignUpComponent,
        title: 'Sign up · VTES Events',
      },
      {
        path: 'thank-you',
        component: SignUpThankYouComponent,
        title: 'Thank You · VTES Events',
      },
      {
        path: 'verify',
        component: SignUpVerifyComponent,
        title: 'Verify · VTES Events',
      },
    ],
  },

  {
    path: 'forgot_password',
    component: ForgotPasswordComponent,
    title: 'Forgot Password · VTES Events',
  },
  {
    path: 'forgot_password',
    component: ForgotPasswordComponent,
    title: 'Forgot Password · VTES Events',
  },
  {
    path: 'profile',
    component: UserProfileComponent,
    title: 'Profile · VTES Events',
    canActivate: [() => inject(CanActivateUser).canActivate()],
  },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    TranslocoModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    RecaptchaModule,
  ],
  providers: [
    { provide: TRANSLOCO_SCOPE, useValue: 'users' },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: environment.recaptcha.siteKey } as RecaptchaSettings,
    },
  ],
  declarations: [
    SignUpComponent,
    SignInComponent,
    ForgotPasswordComponent,
    SignUpVeknIdDialogComponent,
    UserProfileComponent,
    SignUpThankYouComponent,
    SignUpVerifyComponent,
  ],
})
export class UsersModule {}
