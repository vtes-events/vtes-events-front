import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import {
  Observable,
  retryWhen,
  concatMap,
  delay,
  throwError,
  of,
  tap,
  switchMap,
} from 'rxjs';
import { UserService } from './shared/state';

export const retryCount = 5;
export const retryWaitMilliSeconds = 5000;

@Injectable()
export class HttpMonitorInterceptor implements HttpInterceptor {
  constructor(private userService: UserService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      retryWhen((errors) =>
        errors.pipe(
          tap((error) => {
            if (error.status !== 503 && error.status !== 0) {
              throw error;
            }
          }),
          delay(retryWaitMilliSeconds),
          concatMap((error, count) => {
            if (count < retryCount) {
              console.warn(
                `Error status ${error.status}, retry ${count + 1}...`
              );
              return of(error);
            }
            return throwError(() => error);
          })
        )
      ),
      tap({
        next: () => {},
        error: (error) => {
          if (error.status === 401) {
            console.warn('Token invalid or expired, tying to renew...');
            return this.userService
              .checkAccessToken()
              .pipe(switchMap(() => next.handle(request)));
          } else if (error.status === 503) {
            return next.handle(request);
          } else {
            throw error;
          }
        },
      })
    );
  }
}
