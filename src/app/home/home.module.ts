import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { TRANSLOCO_SCOPE, TranslocoModule } from '@ngneat/transloco';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    title: 'VTES Events',
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    TranslocoModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [{ provide: TRANSLOCO_SCOPE, useValue: 'home' }],
  declarations: [HomeComponent],
})
export class HomeModule {}
