import { ApiDataService } from './../shared/services/api.data.service';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DateRange } from '@angular/material/datepicker';
import { ApiTournament, MediaService } from '../shared';
import { Observable, filter, map, startWith, switchMap } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import * as moment from 'moment';

@UntilDestroy()
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsComponent implements OnInit {
  isMobileOrTablet$!: Observable<boolean>;
  todayEvents$!: Observable<ApiTournament[]>;
  upcomingEvents$!: Observable<ApiTournament[]>;

  dateForm!: FormGroup;

  constructor(
    private mediaService: MediaService,
    private apiDataService: ApiDataService
  ) {}

  ngOnInit() {
    //TODO: datepicker with locale
    this.isMobileOrTablet$ = this.mediaService.observeMobileOrTablet();
    const start = moment().add(1, 'days');
    const end = moment(start).add(14, 'days');
    this.dateForm = new FormGroup({
      start: new FormControl<Date | null>(start.toDate(), Validators.required),
      end: new FormControl<Date | null>(end.toDate(), Validators.required),
    });
    this.getActiveEvents();
    this.getUpcomingEvents();
  }

  getActiveEvents(): void {
    const today = new Date();
    this.todayEvents$ = this.apiDataService
      .getTournaments(today, today, 0, 100)
      .pipe(
        untilDestroyed(this),
        map((res) => res.results)
      );
  }

  getUpcomingEvents(): void {
    this.upcomingEvents$ = this.dateForm.valueChanges.pipe(
      untilDestroyed(this),
      startWith({
        start: this.dateForm.value.start,
        end: this.dateForm.value.end,
      }),
      filter((value) => value.start && value.end),
      switchMap((value) =>
        this.apiDataService.getTournaments(value.start, value.end, 0, 1000)
      ),
      map((res) => res.results)
    );
  }

  getSelectedDate(): DateRange<Date> {
    return new DateRange(this.dateForm.value.start, this.dateForm.value.end);
  }

  rangeChanged(currentDate: Date) {
    if (!this.dateForm.value?.start || this.dateForm.value?.end) {
      this.dateForm.patchValue({
        start: currentDate,
        end: null,
      });
    } else {
      const start = this.dateForm.value.start;
      const end = currentDate;
      if (end < start) {
        this.dateForm.patchValue({
          start: end,
          end: start,
        });
      } else {
        this.dateForm.patchValue({
          start: start,
          end: end,
        });
      }
    }
  }

  getDatesInRange(): Date[] {
    const dateArray = new Array();
    const currentDate = new Date(this.dateForm.value.start);
    while (currentDate <= this.dateForm.value.end) {
      dateArray.push(new Date(currentDate));
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
  }

  trackByIndex(index: number, _: any) {
    return index;
  }

  getDateEvents(
    date: Date,
    events: ApiTournament[]
  ): ApiTournament[] | undefined {
    const dateEvents = events.filter((event: ApiTournament) =>
      moment(event.date, 'YYYY-MM-DD').isSame(moment(date), 'date')
    );
    return dateEvents.length ? dateEvents : undefined;
  }

  onLoadMore(): void {
    const start = this.dateForm.value.start;
    const end = this.dateForm.value.end;
    const newEnd = moment(end).add(14, 'days');
    this.dateForm.patchValue({
      start: start,
      end: newEnd.toDate(),
    });
  }
}
