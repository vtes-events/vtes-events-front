import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ApiTournament, MediaService } from '../../../shared';
import { UserQuery } from '../../../shared/state';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventCardComponent {
  @Input() event!: ApiTournament;

  isMobile$ = this.mediaService.observeMobile();

  userVeknId$ = this.userQuery.selectVeknId();

  constructor(
    private mediaService: MediaService,
    private userQuery: UserQuery
  ) {}
}
