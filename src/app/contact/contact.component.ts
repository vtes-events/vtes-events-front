import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiDataService } from '../shared';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { RecaptchaComponent } from 'ng-recaptcha';

@UntilDestroy()
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactComponent implements OnInit {
  @ViewChild('captchaRef') captchaRef!: RecaptchaComponent;
  form!: FormGroup;
  error = false;
  successful = false;

  constructor(
    private apiDataService: ApiDataService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      message: new FormControl(null, Validators.required),
    });
  }

  get name() {
    return this.form.get('name');
  }

  get email() {
    return this.form.get('email');
  }

  get message() {
    return this.form.get('message');
  }

  contact(captchaToken: string): void {
    if (!captchaToken) {
      return;
    }
    this.apiDataService
      .contact(
        this.name!.value,
        this.email!.value,
        this.message!.value,
        captchaToken
      )
      .pipe(untilDestroyed(this))
      .subscribe({
        complete: () => {
          this.message?.reset();
          this.message?.setErrors(null);
          this.successful = true;
          this.error = false;
        },
        error: () => {
          this.error = true;
          this.successful = false;
          this.captchaRef.reset();
        },
      })
      .add(() => {
        this.changeDetectorRef.detectChanges();
      });
  }
}
