import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { debounceTime, filter, tap } from 'rxjs';
import { ApiDataService, ApiShowTypePolicy, ApiTournament } from '../../shared';
import * as moment from 'moment';
import { Router } from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'app-event-creation',
  templateUrl: './event-creation.component.html',
  styleUrls: ['./event-creation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventCreationComponent implements OnInit {
  form!: FormGroup;
  error?: HttpErrorResponse;
  eventLoading = false;
  showTypePolicy = ApiShowTypePolicy;

  constructor(
    private apiDataService: ApiDataService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      eventId: new FormControl(null, [Validators.required]),
      organizerVeknId: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      email: new FormControl(null, [Validators.email]),
      description: new FormControl(null),
      date: new FormControl<Date | null>(null, [Validators.required]),
      time: new FormControl(null, [Validators.required]),
      venueName: new FormControl(null),
      address: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      country: new FormControl(null, [Validators.required]),
      registrationFee: new FormControl(null, [Validators.required]),
      proxiesAllowed: new FormControl(true),
      rounds: new FormControl(null, [Validators.required]),
      phoneNumber: new FormControl(null),
      website: new FormControl(null),
      eventType: new FormControl(null),
      registrationOpen: new FormControl(true),
      showDeckPolicy: new FormControl(ApiShowTypePolicy.N),
    });
    this.onEventIdChange();
  }

  get eventId() {
    return this.form.get('eventId');
  }

  get organizerVeknId() {
    return this.form.get('organizerVeknId');
  }

  get name() {
    return this.form.get('name');
  }

  get email() {
    return this.form.get('email');
  }

  get description() {
    return this.form.get('description');
  }

  get date() {
    return this.form.get('date');
  }

  get time() {
    return this.form.get('time');
  }

  get venueName() {
    return this.form.get('venueName');
  }

  get address() {
    return this.form.get('address');
  }

  get city() {
    return this.form.get('city');
  }

  get country() {
    return this.form.get('country');
  }

  get registrationFee() {
    return this.form.get('registrationFee');
  }

  get proxiesAllowed() {
    return this.form.get('proxiesAllowed');
  }

  get rounds() {
    return this.form.get('rounds');
  }

  get phoneNumber() {
    return this.form.get('phoneNumber');
  }

  get website() {
    return this.form.get('website');
  }

  get eventType() {
    return this.form.get('eventType');
  }

  get registrationOpen() {
    return this.form.get('registrationOpen');
  }

  get showDeckPolicy() {
    return this.form.get('showDeckPolicy');
  }

  onCreateEvent() {
    this.apiDataService
      .createTournament({
        ...this.form.value,
        date: moment(this.date?.value).format('YYYY-MM-DD'),
        showDeckPolicy: ApiShowTypePolicy[this.showDeckPolicy?.value],
      })
      .pipe(untilDestroyed(this))
      .subscribe({
        next: (tournament) => {
          this.router.navigate([`/events/${tournament.id}`]);
        },
        error: (error: HttpErrorResponse) => {
          this.error = error;
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  private onEventIdChange() {
    this.eventId?.valueChanges
      .pipe(
        untilDestroyed(this),
        debounceTime(500),
        filter((value) => value!),
        tap((value) => {
          this.eventLoading = true;
          this.changeDetectorRef.detectChanges();
          this.apiDataService
            .getVEKNTournament(value)
            .pipe(untilDestroyed(this))
            .subscribe({
              next: (tournament) => {
                this.error = undefined;
                this.eventLoading = false;
                this.setTournamentInfo(tournament);
                this.changeDetectorRef.detectChanges();
              },
              error: (error: HttpErrorResponse) => {
                this.error = error;
                this.eventLoading = false;
                this.changeDetectorRef.detectChanges();
              },
            });
        })
      )
      .subscribe();
  }

  private setTournamentInfo(tournament: ApiTournament) {
    this.organizerVeknId?.patchValue(tournament.organizerVeknId);
    this.name?.patchValue(tournament.name);
    this.date?.patchValue(moment(tournament.date, 'YYYY-MM-DD').toDate());
    this.time?.patchValue(moment(tournament.time, 'hh:mm:ss').format('hh:mm'));
    this.venueName?.patchValue(tournament.venueName);
    this.address?.patchValue(tournament.address);
    this.city?.patchValue(tournament.city);
    this.country?.patchValue(tournament.country);
    this.registrationFee?.patchValue(tournament.registrationFee);
    this.proxiesAllowed?.patchValue(tournament.proxiesAllowed);
    this.rounds?.patchValue(tournament.rounds);
    this.eventType?.patchValue(tournament.eventType);
  }
}
