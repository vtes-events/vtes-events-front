# build angular
FROM node:lts-alpine as build
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --omit=dev

# compose nginx
FROM nginx:stable-alpine
COPY --from=build /app/dist/vtes-events-front /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/conf.d/default.conf
